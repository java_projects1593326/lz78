//Adam Williams 1518245.
//Joseph Smith 1578087.

import java.io.*;
import java.math.*;

public class LZencode{

    public static void main(String[] args){
        try{
            //Gets input stream and puts into byte array.
            InputStream is = System.in;

            int length = is.available();
            byte[] array = new byte[length];
            byte[] decodeArray = new byte[length];
            is.read(array);

            String result = "";
            
            //Turns bytes in array to nibbles.
            for(byte b: array){
                int high = (b & 0xF0) >>> 4;
                int low = b & 0xF;

                String high2 = Integer.toHexString(high);
                String low2 = Integer.toHexString(low);

                result += high2 + low2;
            }
            
            //Creates new multiway trie.
            Trie tr = new Trie();
            String newInput = "";

            //Inserts into trie using LZ78.
            for(int i = 0; i < result.length(); i++){
                char c = result.charAt(i);

                newInput += c;
                if(tr.search(newInput) == false){
                    tr.insert(newInput);
                    newInput = "";
                }
            }
        }
        catch(Exception e){
            System.err.print(e);
        }
}
}
