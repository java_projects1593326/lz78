//Adam Williams 1518245.
//Joseph Smith 1578087.

import java.util.*;

public class Trie{

    //Intilizes root node.
    private TrieNode root;
    //Saves index positions of when character gets inserted into trie.
    int pos = 1;
    //Initilizes trie, and add root node to node list.
    public Trie(){
        root = new TrieNode('\0', 0);
    }
    //inserts into trie
    public void insert(String x){
        TrieNode current = root;
        try{
        //For every character in the string we get the character and insert into the trie.
        for(int i = 0; i < x.length(); i++){
            char c = x.charAt(i);
            int index = Character.digit(c, 16);
            //If current linked list has character add new node to list then get children of new node.
            if(current.children[index] == null){
                current.children[index] = new TrieNode(c, pos);
                parentPrint(current, index);
            }
            current = current.children[index];
        }
        pos ++;
        current.isLeaf = true;
        }
        catch(Exception e){
            System.err.println(e);

        }

    }

    //Searches to see if string is in the trie.
    public boolean search(String x){
        TrieNode  current = root;

        //For every character in the string.
        for(int i = 0; i < x.length(); i++){
            char c = x.charAt(i);
            int index = Character.digit(c, 16);
            //If current linked list has character add new node to list then get children of new node.
            if(current.children[index] == null){
                return false;
            }
            current = current.children[index];
        }
        return current.isLeaf;
       
    }
    
    //Prints out the two integer pairs dictionary.
    public void parentPrint(TrieNode node, int index){
        TrieNode children = node.children[index];


        System.out.println(Integer.toString(node.index) + "," +Character.toString(children.c));
    }
}