//Adam Williams 1518245.
//Joseph Smith 1578087.

import java.io.*;
import java.math.*;
import java.util.*;

public class LZdecode{
    public static void main(String[] args){
        try{
        
        //Initilisese input streams.
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(isr);
        //Initializes arrays.
        ArrayList<String> inputArray = new ArrayList<String>();
        ArrayList<String> dictionary = new ArrayList<String>();
        String output = "";
        String line = "";

        //First input in array in null.
        inputArray.add(null);
        int pos = 1;

        //Reads input stream.
        while((line = reader.readLine()) != null){
            //System.out.println(line);
            inputArray.add(line);
            pos ++;

        }

        for(String s : inputArray){
            System.out.println(s);
        }
        
        //First string in dictionary is null.
        dictionary.add("");
        String phrase = "";
        
        //Populates the dictionary with input from input array.
        for(int i = 1; i < inputArray.size(); i++){
            String entry = inputArray.get(i);
            String[] array = entry.split(",");

            if(array[0] == "0"){
                dictionary.add(array[1]);
            }
            else{
                String position = dictionary.get(Integer.valueOf(array[0]));
                phrase = position + array[1];
                dictionary.add(phrase);
                phrase = "";
            }
        }

        //Puts dictionary result into one string.
        String result = "";
        for(String s : dictionary){
            result += s;
        }

        //Converts bytes to string and prints result.
        byte[] bytes = hexToByte(result);
        String st = new String(bytes);
        System.out.print(st);

        reader.close();

        }

        catch(Exception e){
            System.err.println(e);
        }
    }

    //Converts hex string to bytes.
    public static byte[] hexToByte(String x){
        
        int l = x.length();

        byte[] data = new byte[l / 2];

        for(int i = 0; i < l ; i +=2){
            data[i/2] = (byte) ((Character.digit(x.charAt(i), 16) << 4) + Character.digit(x.charAt(i + 1), 16));
        }
        return data;
    }


}