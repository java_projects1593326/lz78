//Adam Williams 1518245.
//Joseph Smith 1578087.

public class TrieNode{

    //Initilizes properties of TrieNode.
    char c;
    TrieNode[] children;
    boolean isLeaf;
    int index;

    //Takes in character, index and creates an array of children for that node.
    public TrieNode(char c, int index){
        this.c = c;
        this.isLeaf = false;
        this.children = new TrieNode[16];
        this.index = index;
    }
    //Peeks at current character.
    public char peek(){
    	return this.c;
    }

}
